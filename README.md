# Configuration Normalizer

Configuration Normalizer processes configuration to prepare it for comparison.

## Developer usage

This module can be used to wrap any configuration storage, creating a read-only
version of the storage for which any data read will be returned in a normalized
form.

The most common usage would be to minimize non-meaningful differences when
comparing configuration data from different sources. Configuration Normalizer
provides a trait to facilitate this.

For full information visit the project page, [Config Normalizer](https://www.drupal.org/project/config_normalizer).

And [see or file issues](https://www.drupal.org/project/issues/config_normalizer).

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Acknowledgements

This module draws significantly on work by Jennifer Hodgdon in Configuration
Update Manager and Fabian Bircher in Config Filter.


## Maintainers

- Joe Parsons- [joegraduate](https://www.drupal.org/u/joegraduate)
- tadean - [tadean](https://www.drupal.org/u/tadean)
- Nedjo Rogers - [nedjo](https://www.drupal.org/u/nedjo)
- Artem Dmitriiev - [a.dmitriiev](https://www.drupal.org/u/admitriiev)
- Fabian Bircher - [bircher](https://www.drupal.org/u/bircher)
- Merlin Axel Rutz - [geek-merlin](https://www.drupal.org/u/geek-merlin)
